# Introdução {#sec:introducao}

O universo de informação que pretendemos retratar é da _Contratação Pública_,
que, para o caso vertente, é retratada por duas entidade: _Adjudicante_ e
_Adjudicatária_ que celebram entre si um _Contrato_.

Por forma a realizar um bom trabalho consideramos necessário o estudo exaustivo
das temáticas abordadas no trabalho, quer a _Programação em Lógica Estendida_,
quer a _Representação de Conhecimento Imperfeito_, quer a legislação da
Contratação Pública (esta última de uma forma não tão exaustiva, uma vez que não
é o foco primordial do trabalho). O estudo de parte da legislação inerente à
_Contratação Pública_ acrescenta valor ao trabalho porque permitir-nos-á a
adição de algumas regras de inclusão, remoção e manutenção de conhecimento.
