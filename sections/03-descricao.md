# Descrição do trabalho e análise de resultados {#sec:descricao}

Uma vez que `Prolog` é a linguagem de programação lógica que utilizamos nesta
unidade curricular, a expressão das regras e conceitos enumerados na secção
anterior foram implementados usando essa mesma ferramenta.

Em relação à estrutura do projeto optamos por separar o desenvolvimento dos três
predicados (`adjudicante`, `adjudicataria`, `contrato`) em três ficheiros
diferentes (`adjudicante.pl`, `adjudicataria.pl`, `contrato.pl`) e através do
predicado `include/1` (que inclui textualmente o conteúdo do ficheiro argumento
na linha onde é chamado o predicado). Por forma a unificar a informação presente
em cada um dos ficheiros incluímos cada um deles no ficheiro `concursos.pl` para
que o utilizador possa ter acesso a toda a informação que consta da base de
conhecimento carregando apenas um ficheiro.

Para carregar a totalidade da base de conhecimento basta executar, na pasta
origem do projeto: `bin/run src/concursos.pl`, em sistemas _UNIX_. Caso seja
utilizador de Windows, por exemplo, deve seguir o procedimento que utilizada
normalmente e carregar o ficheiro `concursos.pl` que se encontra na pasta `src`.

As regras listadas anteriormente servem para gerir a manipulação de conhecimento
na base de conhecimento (que no nosso caso dizem respeito aos predicados
`adicionar` e `remover` definidos no ficheiro `utils.pl`). Vamos, então analisar
a implementação das regras para cada um dos predicados em _Prolog_.

## Adjudicante

Para implementar a regra relativa à adição de factos do predicado `adjudicante`
podemos implementar invariantes relativos a essa mesma adição, sendo que a
ideia é bastante simples: após a inserção de um predicado `adjudicante(IdAd,
Nome, NIF, Morada)` apenas pode existir um ou menos predicado na base de
conhecimento quer com _IdAd_, quer com _Nome_ quer com _NIF_. Noutras palavras:
o _IdAd_, o _Nome_ e o _NIF_ de cada adjudicante são identificadores únicos.

```prolog
% Garante que o _IdAd_ de cada adjudicante é único
+adjudicante(IdAd, _, _, _) ::
  (solucoes((IdAd), adjudicante(IdAd, _, _, _), S),
  comprimento(S, N),
  N =< 1).

% Garante que o _Nome_ de cada adjudicante é único
+adjudicante(_, Nome, _, _) ::
  (solucoes((Nome), adjudicante(_, Nome, _, _), S),
  comprimento(S, N),
  N =< 1).

% Garante que o _NIF_ de cada adjudicante é único
+adjudicante(_, _, NIF, _) ::
  (solucoes((NIF), adjudicante(_, _, NIF, _), S),
  comprimento(S, N),
  N =< 1).
```

Por outro lado, para implementar a regra relativa à remoção de factos relativos
ao mesmo predicado basta: após a remoção o número de contratos que tenham como
_IdAd_ o campo correspondente do predicado apagado tem de ser igual a zero, caso
contrário a mudança tem de ser revertida.

```prolog
-adjudicante(IdAd, _, _, _) ::
    (solucoes((IdAd), (contrato(IdAd, _, _, _, _, _, _, _, _, _)), S),
    comprimento(S, N),
    N == 0).
```

## Adjudicatária

O raciocínio subjacente à implementação das regras de remoção de inserção de
conhecimento relativo ao predicado `adjudicataria` é análogo em ambos os casos
ao predico `ajudicante`:

```prolog
% Garante que o _IdAda_ de cada adjudicataria é único
+adjudicante(IdAda, _, _, _) ::
  (solucoes((IdAda), adjudicataria(IdAda, _, _, _), S),
  comprimento(S, N),
  N =< 1).

% Garante que o _Nome_ de cada adjudicataria é unico
+adjudicataria(_, Nome, _, _) ::
  (solucoes((Nome), adjudicataria(_, Nome, _, _), S),
  comprimento(S, N),
  N =< 1).

% Garante que o _NIF_ de cada adjudicataria é unico
+adjudicataria(_, _, NIF, _) ::
  (solucoes((NIF), adjudicataria(_ ,_, NIF, _), S),
  comprimento(S, N),
  N =< 1).
```

O princípio subjacente ao invariante da remoção é também análogo à entidade
`adjudicante`.

```prolog
-adjudicante(IdAda, _, _, _) ::
    (solucoes((IdAda), contrato(_, IdAda, _, _, _, _, _, _, _, _), S),
    comprimento(S, N),
    N == 0).
```

## Contrato

Um contrato referencia uma entidade _Adjudicante_ e uma entidade _Adjudicataria_
pelo que caso estas se encontrem no contrato é imperativo que estejam registadas
na base de conhecimento. Deste modo, surge o seguinte invariante que garante as
condições anteriores e garante ainda que após a inserção do contrato existe
apenas um contrato que o respetivo identificador.

```prolog
+contrato(Id, IdAd, IdAda, _, _, _, _, _, _, _) ::
    (existeApenasContrato(Id),
    adjudicanteInKB(IdAd),
    adjudicatariaInKB(IdAda)).
```

O próximo invariante garante as regras relativas aos contratos públicos
atribuídos por "Ajuste Direto". Tem de garantir que caso o tipo de procedimento
do contrato seja "Bens e Serviços" então o contrato apenas será, com sucesso,
introduzido na base de conhecimento caso o valor do mesmo não ultrapassa os
75000,00€. No caso do tipo de procedimento ser "Empreitada de Obras Públicas"
então o valor do contrato não pode ultrapassar os 150000,00€. Por último, caso
estejamos perante um tipo de procedimento que não encaixa em nenhum dos
anteriores o valor do contrato não pode ultrapassar os 100000,00€. (Esta última
clausula configura uma pequena simplificação da legislação).

```prolog
+contrato(_, _, _, TipoProc, 'Ajuste Direto', _, Valor, _, _, _) ::
    ((TipoProc == 'Bens e Servicos',
     Valor =< 75000);
     (TipoProc == 'Empreitada de Obras Publicas',
     Valor =< 150000);
     (TipoProc \= 'Bens e Servicos',
     TipoProc \= 'Empreitada de Obras Publicas',
     Valor =< 100000)).
```

O seguinte invariante garante as regras relativas aos contratos públicos
atribuídos por "Concurso Público de Âmbito Nacional". No caso de o tipo de
procedimento ser "Empreitada de Obras Públicas" tem de garantir que o valor do
contrato não ultrapassa os 5225000,00€. Por outro lado, caso o tipo de
procedimento seja "Bens e Serviços" o valor do contrato é limitado a 418000,00€.

```prolog
+contrato(_, _, _, TipoProc, 'Concurso Publico de Ambito Nacional', _, Valor,
    _, _, _) ::
      ((TipoProc = 'Empreitada de Obras Publicas',
       Valor =< 5225000);
       (TipoProc = 'Bens e Servicos',
       Valor =< 418000)).
```

A regra que corresponde ao seguinte invariante encontra-se referenciada no
enunciado como relativa aos contratos atribuídos por "Ajuste Direto". No
entanto, após a análise da legislação constatamos que esta regra diz, na
verdade, respeito a contratos atribuídos por "Ajuste Direto Simplificado", pelo
que decidimos refletir essa alteração no trabalho. Assim, para contratos
atribuídos por "Ajuste Direto Simplificado" podem apenas contemplar os seguintes
tipos de procedimentos: "Aquisição de Bens Móveis", "Locação de Bens Móveis"e
"Aquisição de Serviços". Como este tipo de contratos tem um prazo máximo de 1
ano e o tempo de duração é expresso em dias, o campo _Prazo_ tem o valor máximo
de 365 dias. Por último, o valor do contrato não pode exceder os 5000,00€.

```prolog
+contrato(_, _, _, TipoContrato, 'Ajuste Direto Simplificado', _, Valor, Prazo,
    _, _ ) ::
    (Valor =< 5000,
    (TipoContrato = 'Aquisicao de Bens Moveis';
     TipoContrato = 'Locacao de Bens Moveis';
     TipoContrato = 'Aquisicao de Servicos'),
    Prazo =< 365).
```

A regra dos três anos (que se aplica a todos os contratos) é garantida pelo
invariante abaixo. O raciocínio subjacente a produção do invariante é:
primeiramente introduziste o contrato na base de conhecimento. De seguida,
procuram-se os contratos do mesmo tipo realizados entre as mesmas entidades
(_adjudicante_ e _adjudicataria_), guardando-se o seu valor e data numa lista.
Seguidamente, filtram-se os pares _(Valor, Data)_ que se enquadram nos últimos
três ano (relativamente à data de celebração do contrato que se quer
introduzir). Por último soma-se o valor dos contratos restantes. Se o valor
obtido na soma for superior a 75000,00€ a alteração deve ser revertida, isto é,
o contrato deve ser removido da base de conhecimento.

```prolog
+contrato(_, IdAd, IdAda, TipoContrato, _, _, Valor, _, _, Data) ::
    (solucoes((V),(contrato(_, IdAd, IdAda, TipoContrato, _, _, V, _, _, D),
    lastThreeYears(D, Data)),R),
    sumList(R, S),
    S < 75000 + Valor).
```

## Sistema de Inferência

```prolog
demo(P, verdadeiro) :-
    P,
    nao(excecao(P)),
    nao(nao(P)).
demo(P, desconhecido) :-
    excecao(P).
demo(P,falso) :-
    nao(P),
    nao(excecao(P)).
```

Para que um predicado seja interpretado como `VERDADEIRO` este tem de constar da
base de conhecimento, não pode constar da mesma uma exceção relativa a esse
predicado e não pode existir uma declaração negativa do predicado.

No caso de um predicado ser interpretado como `FALSO` este tem de estar
declarado na base de conhecimento como conhecimento negativo e não pode existir
uma exceção relativa ao predicado.

Por último, para que um predicado seja interpretado como `DESCONHECIDO` tem de
constar da base de conhecimento a declaração da exceção relativa ao predicado.

## Bateria de Testes

De modo a podermos ter confiança nas implementações que íamos adicionando
criamos uma bateria de testes unitários que permitem atestar que as
operações estão a ser devidamente efetuadas. Implementamos testes para todos os
predicados abordados neste trabalho (`adjudicante`, `adjudicataria` e
`contrato`).

Existem, na nossa implementação, três tipos fundamentais de testes:

Os testes que dependem do sistema inferência, que apresentam a seguinte
estrutura:

```prolog
templateTest(numero_teste) :-
    demo(predicado(teste), R),
    R == resultado_pretendido,
    write('Sucesso \n');
    write('Insucesso \n').
```

Os testes que avaliam a introdução de novos factos na base de conhecimento:

```prolog
templateTest(numero_teste) :-
   adicionar(predicado(teste), R),
   R == resultado_pretendido,
   write('Sucesso \n');
   write('Insucesso \n').
```

Os testes a remoção de factos da base de conhecimento:

```prolog
templateTest(numero_teste) :-
  remover(predicado(teste), R),
  R == resultado_pretendido,
  write('Sucesso \n');
  write('Insucesso \n').
```

A ideia subjacente ao teste é: passar o termo pelo sistema de inferência,
verificar se o resultado obtido é aquele que esperávamos e caso afirmativo
imprimimos "Sucesso" para o terminal, ou "Insucesso", caso contrário.

Os testes podem ser corridos através do comando: `bin/tests` a partir da pasta
origem do projeto, em sistemas _UNIX_. Caso se encontre noutro sistema operativo
pode carregar os ficheiros relativos a cada módulo de testes e correr o
predicado: `run_tests(1).`.
