# Conclusões e sugestões {#sec:sugestoes}

A base de conhecimento desenvolvida é o resultado da aplicação dos conhecimentos
sobre _Programação em Lógica Estendida_ e _Representação de Conhecimento
Imperfeito_. Para a implementação da base de conhecimento tentamos ser tão fieis
quanto possível à legislação que vigora no código da atribuição de contratos
públicos. Deixamos de fora algumas distinções, como por exemplo, a distinção
entre _Organismos pertencentes ao setor público administrativo tradicional_ e
_Organismos de direito público_ no que diz respeito às entidades adjudicantes
por considerarmos um nível de detalhe que não se justifica perante o objetivo
principal da realização deste trabalho, que é o desenvolvimento das competências
de representação de conhecimento.

A bateria de testes adicionada confere maior segurança no desenvolvimento do
projeto uma vez que a cada iteração pode ser atestada a consistência da base de
conhecimento e a preservação das regres que regem quer adição quer a remoção de
conhecimento.

Todos os requisitos do trabalho foram cumpridos: construímos um caso prático da
aplicação dos conhecimentos adquiridos, ou seja, temos uma base de conhecimento
que constituí um subconjunto da base de dados de contratação pública e que
configura um exemplo de representação de conhecimento. Representamos
conhecimento positivo e negativo (em todos os predicados), representamos casos
de conhecimento imperfeito recorrendo a todos os tipos de valores nulos
estudados. Constituímos invariantes que regem a inserção e remoção de
conhecimento no sistema. Lidamos, com sucesso, com a problemática de evolução e
involução de conhecimento implementando os procedimentos. Por último,
desenvolvemos um sistema de inferência capaz de avaliar predicados, de acordo
com a conhecimento que constitui a base de conhecimento.
