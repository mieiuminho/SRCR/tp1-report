# Preliminares {#sec:preliminares}

Qualquer sistema que necessita de lidar informação necessita de um mecanismo e
de um paradigma para a armazenar. Até agora conhecíamos apenas um paradigma:
aquele que verificamos nas bases de dados tradicionais (_e.g_ relacionais ou
documentais, por exemplo), que, conforme percebemos agora, apresenta algumas
limitações principalmente no modo como a ausência de informação explicitamente
representada é interpretada. Existe um outro paradigma: o que usamos nesta
unidade curricular e que permite representar informação completa, que no
presente contexto quer dizer que conseguimos, por exemplo, representar
intervalos de valores para os quais a informação poderá ser verdadeira, sendo
que valores fora desse intervalo são considerados falsos.

Por forma a entender esta temática em toda a sua extensão temos de entender os
pressupostos em que podem acertar as linguagens e sistemas de manipulação de
bases de dados:

- **Pressuposto do Mundo Fechado** - toda a informação que não consta
  explicitamente da base de dados é considerada falsa.

- **Pressuposto dos Nomes Únicos** - duas constantes diferentes (que definam
  valores atómicos ou objetos) designam, necessariamente, duas entidades
  diferentes do universo do discurso.

- **Pressuposto do Domínio Fechado** - não existem mais objetos no universo do
  discurso para além daqueles designados por constantes na base de dados.

É relativamente fácil de perceber que para implementar o paradigma da
_Representação de Conhecimento Imperfeito_ temos de abandonar estes
pressupostos, uma vez que: o primeiro pressuposto não permitiria a evolução da
base de conhecimento; o terceiro pressuposto inviabilizaria a interpretação do
_Conhecimento Imperfeito_. Vamos, então, adotar um novo conjunto de
pressupostos:

- **Pressuposto do Mundo Aberto** - podem existir outros factos ou conclusões
  verdadeiros para além daqueles representados na base de conhecimento.

- **Pressuposto dos Nomes Únicos** - duas constantes diferentes (que definam
  valores atómicos ou objetos) designam, necessariamente, duas entidades
  diferentes do universo de discurso.

- **Pressuposto do Domínio Aberto** - podem existir mais objetos do universo de
  discurso para além daqueles designados pelas constantes da base de conhecimento.

Para a implementação do sistema que o enunciado do trabalho menciona precisamos
de estender a programação lógica. Um programa lógico tradicional avalia
predicados fornecendo um output equivalente a `Verdadeiro` ou `Falso`. No
entanto, conforme já referimos, neste trabalho propomo-nos a mais do que isso
portanto estenderemos a programação lógica de modo a que esta possa fornecer
_outputs_ equivalentes a `Verdadeiro`, `Falso` ou `Desconhecido`.

Assim, chegamos a um conceito absolutamente estruturante desta temática: a
representação dos valores nulos (que circunstanciam o conceito de _Conhecimento
Imperfeito_). A representação dos valores nulos preconiza uma estratégia para
enumerar os casos para os quais se pretende fazer a distinção em que as
respostas a questões deverão ser concretizadas como `Verdadeiro`, `Falso` ou
`Desconhecido`.

Existem três tipos de valores nulos:

- **Incerto** - quando o valor é desconhecido e se enquadra num conjunto
  indeterminado de hipóteses.

- **Impreciso** - quando o valor é desconhecido mas se enquadra num conjunto
  determinado de hipóteses.

- **Interdito** - desconhecido e não permitido saber.

O estudo do _Conhecimento Imperfeito_ termina com o entendimento destes
conceitos. Para que possamos realizar um trabalho em que tratamos a realidade
com tanto detalhe com tanto detalhe quanto possível estudamos o _Guia da
Contratação Pública_ e, por isso, iremos enumerar as regras cujas implementação
consideramos justificada no contexto deste projeto.

Conforme sugerido no enunciado, existem três predicados principais:

- `adjudicante(IdAd, Nome, NIF, Morada)` $\rightarrow {V, F, D}$

- `adjudicataria(IdAda, Nome, NIF, Morada)` $\rightarrow {V, F, D}$

- `contrato(IdCon, IdAd, IdAda, TipoDeContrato, TipoDeProcedimento, Descrição, Valor, Prazo, Local, Data)` $\rightarrow {V, F, D}$

Percorreremos, então, os predicados listados pela mesma ordem que foram
apresentados:

## Adjudicante

1. Consideramos que um predicado (_adjudicante(IdAd, Nome, NIF, Morada)_) não
   pode ser adicionado à base de conhecimento se desta já constar a combinação
   _(IdAd, Nome, NIF)_ que caraterizam o predicado.

2. Consideramos que um predicado (_adjudicante(IdAd, Nome, NIF, Morada)_) não
   pode ser removido da base de conhecimento se desta constar um contrato em que
   o identificador do adjudicante é igual a _IdAd_.

## Adjudicatária

1. Consideramos que um predicado (_adjudicataria(IdAda, Nome, NIF, Morada)_) não
   pode ser adicionado à base de conhecimento se desta já constar a combinação
   _(IdAda, Nome, NIF)_ que caraterizam o predicado.

2. Consideramos que um predicado (_adjudicataria(IdAda, Nome, NIF, Morada)_) não
   pode ser removido da base de conhecimento se desta constar um contrato em que
   o identificador da entidade adjudicatária é igual a _IdAda_.

## Contrato

O seguinte conjunto de regras deriva da nossa interpretação do seguinte quadro
que consta do _Guia da Contratação Pública_ que estabelece uma relação entre o
valor do contrato e o tipo de contrato e o tipo de procedimentos envolvidos na
sua adjudicação.

![Relação entre valores e o tipo de procedimento](figures/valor_procedimento.png)

![Limte de valores por procedimento](figures/valores.png)

Destes quadros depreendemos as seguintes regras:

Para contratos atribuídos por **'Ajuste Direto'**:

1. Se o tipo de contrato for **'Bens e Serviços'**, então o valor máximo do
   contrato é 75000,00€.

2. Se o tipo de contrato for **'Empreitada de Obras Públicas'**, então o valor
   máximo do contrato é 150000,00€.

3. Se o tipo de contrato não nenhum dos acima descritos, então o valor máximo
   do contrato é 100000,00€.

Para contratos atribuídos por **'Concurso Público de Âmbito Nacional'**:

1. Se o tipo de contrato for **'Empreitada de Obras Públicas'** então o valor
   máximo do contrato é 5225000,00€.

2. Se o tipo de contrato for **'Bens e Serviços'** então o valor máximo do
   contrato é 418000,00€.

Após a leitura do _Guia da Contratação Pública_ constatamos que a regra presente
no enunciado do trabalho referente ao valor máximo do contrato, assim como o
prazo de vigência refere-se, na verdade, ao processo de atribuição **'Ajuste
Direto Simplificado'**, pelo que achamos apropriada que essa pequena correção se
traduzisse na seguinte regra:

1. Para contratos atribuídos por **'Ajuste Direto Simplificado'**, o valor
   máximo do mesmo é 5000,00€, tem de ser do tipo **'Aquisição de Bens
   Móveis'**, **'Locação de Bens Móveis'** ou **'Aquisição de Serviços'** e o
   seu prazo de vigência máximo é 1 ano (365 dias).

Por fim, adicionamos, assim como sugerido no documento de apoio, a regra dos 3
anos para todos os contratos:

1. Uma entidade adjudicante não pode convidar a mesma empresa para celebrar um
   contrato com prestações de serviço do mesmo tipo ou idênticas às de contratos
   que já lhe foram atribuídos, no ano económico em curso e nos dois anos
   económicos, sempre que:

   - O preço contratual acumulado dos contratos já celebrados (não incluindo o
     contrato que se pretende celebrar) seja igual ou superior a 75000,00€.
