\begin{abstract}
O presente relatório descreve as fases de desenvolvimento e o estudos
necessários à realização deste trabalho cujos temas de interesse são
\textit{Programação em Lógica Estendida} e \textit{Representação de
Conhecimento Imperfeito}.
\end{abstract}
